from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    import_href = models.CharField(max_length=200, unique=True)




#2.# Create your models here.
class Hat(models.Model):
    style = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    fabric = models.CharField(max_length=200)
    picture = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_list_hats", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.color} {self.style} {self.fabric}"

    class Meta:
        ordering = ("fabric", "style", "color")
