from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import Shoe, BinVO


class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
    ]

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "bin_number",
    ]

class ShoesDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "colour",
        "pic_url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder()
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):

    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder = ShoesListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin_id = content["bin"]
            bin = BinVO.objects.get(id = bin_id)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin ID."},
                status = 400
            )

        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder = ShoesDetailEncoder,
            safe = False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_shoes(request, id):

    if request.method == "GET":
        shoes = Shoe.objects.filters(id=id)
        return JsonResponse(
            shoes,
            encoder = ShoesDetailEncoder,
            safe = False,
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id = id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "shoes" in content:
                shoes = Shoe.objects.get(id = content["shoes"])
                content["shoes"] = shoes
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Those shoes do not exist."},
                status = 400,
            )

    Shoe.objects.filter(id = id).update(**content)
    shoes = Shoe.objects.get(id = id)
    return JsonResponse(
        shoes,
        encoder = ShoesDetailEncoder,
        safe = False,
    )
